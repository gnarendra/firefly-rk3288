
/*
 * Copyright (C) 2010 The Android Open Source Project
 * Copyright (C) 2012, The Linux Foundation. All rights reserved.
 *
 * Not a Contribution, Apache license notifications and license are
 * retained for attribution purposes only.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cutils/properties.h>
#include <hardware_legacy/uevent.h>
#include <utils/Log.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <string.h>
#include <stdlib.h>
#include<fcntl.h>


#include "hwc.h"

#define UEVENT_DEBUG 0
#define HWC_UEVENT_THREAD_NAME "hwcUeventThread"

static void set_lut(hwc_context_t* ctx,int connected)
{
	int LUT[3][3] = {263,516,100,-152,-298,450, 450,-377,-73};
	int bpp[256]={0};
	if(connected == 0){
		for(int i=0;i<256;i++)
		{
			uint8_t yuv[4];
			uint8_t Y,U,V;
			memcpy(yuv,(void*)&ctx->bpp[i],sizeof(int));
			Y = (LUT[0][0]*(yuv[2]) + LUT[0][1]*(yuv[1]) + LUT[0][2]*(yuv[0])  +16*1024+ 512) >> 10;
			U = (LUT[1][0]*(yuv[2]) + LUT[1][1]*(yuv[1]) + LUT[1][2]*(yuv[0]) + 128*1024 + 512) >> 10;
			V = (LUT[2][0]*(yuv[2]) + LUT[2][1]*(yuv[1]) + LUT[2][2]*(yuv[0]) + 128*1024 + 512) >> 10;
			bpp[i] = (yuv[3]<<24)|(Y<<16)|(U<<8)|V;
			
		}
	}else{
		memcpy(bpp,ctx->bpp,sizeof(bpp));
	}
	char* tmp = NULL;
	int tmpSize = 256*9;
	tmp = malloc(tmpSize);
	memset(tmp,0,tmpSize);
	for(int i=0;i<256;i++){
		char test[9];
		sprintf(test,"%08x",bpp[i]);	
		strcat(tmp,test);
		if(i!=255)
			strcat(tmp," ");
	}

	int mret = -1;
	mret = open("/sys/class/graphics/fb0/hwc_lut",O_RDWR);
	write(mret,tmp,strlen(tmp));
	close(mret);
	if(tmp != NULL){
		free(tmp);
		tmp = NULL;
	}
}

static void handle_uevent(hwc_context_t* ctx, const char* udata, int len)
{
    int vsync = 0;
    int64_t timestamp = 0;
    const char *str = udata;

    if(!strcasestr("change@/devices/virtual/switch/hdmi", str) ) {
        ALOGD_IF(UEVENT_DEBUG, "%s: Not Ext Disp Event ", __FUNCTION__);
        return;
    }
    int connected = -1; // initial value - will be set to  1/0 based on hotplug
	
    // parse HDMI/WFD switch state for connect/disconnect
    // for HDMI:
    // The event will be of the form:
    // change@/devices/virtual/switch/hdmi ACTION=change
    // SWITCH_STATE=1 or SWITCH_STATE=0
    while(*str) {
        if (!strncmp(str, "SWITCH_STATE=", strlen("SWITCH_STATE="))) {
            connected = atoi(str + strlen("SWITCH_STATE="));
            break;
        }
        str += strlen(str) + 1;
        if (str - udata >= len)
            break;
    }
    ALOGD("%s sending hotplug: connected = %d ",
                      __FUNCTION__, connected);
	
	// We delay 500ms to wait HDMI/AV switch complete.
    usleep(500000);
    ctx->procs->invalidate(ctx->procs);
	
	char property[PROPERTY_VALUE_MAX];
	memset(property, 0, PROPERTY_VALUE_MAX);
	property_get("persist.sys.use.tve_test", property, "0");
	if(atoi(property) > 0)
    	set_lut(ctx,connected);
}

static void *uevent_loop(void *param)
{
    int len = 0;
    static char udata[PAGE_SIZE];
    hwc_context_t * ctx = reinterpret_cast<hwc_context_t *>(param);
    char thread_name[64] = HWC_UEVENT_THREAD_NAME;
    prctl(PR_SET_NAME, (unsigned long) &thread_name, 0, 0, 0);
    setpriority(PRIO_PROCESS, 0, HAL_PRIORITY_URGENT_DISPLAY);
    uevent_init();

    while(1) {
        len = uevent_next_event(udata, sizeof(udata) - 2);
        handle_uevent(ctx, udata, len);
    }

    return NULL;
}

void init_uevent_thread(hwc_context_t* ctx)
{
    pthread_t uevent_thread;
    int ret;

    ALOGD("Initializing UEVENT Thread");
    ret = pthread_create(&uevent_thread, NULL, uevent_loop, (void*) ctx);
    if (ret) {
        ALOGE("%s: failed to create %s: %s", __FUNCTION__,
            HWC_UEVENT_THREAD_NAME, strerror(ret));
    }
}

